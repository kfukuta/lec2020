/*
	ppps_int_slist.h
	[サンプルライブラリ] int型の単方向線形リスト
	
	author:  K.Y (Programming Place Plus)
	
	version  1.0.2   '2019/9/16
               ・成否を返す関数の戻り値に、bool型を使うようにした
             
             1.0.0   '2012/1/4
*/

#ifndef PPPS_INT_SLIST_H
#define PPPS_INT_SLIST_H

#include <stdbool.h>

/*
	単方向線形リスト型
*/
struct PPPSIntSlist_tag {
	int                        value;
	struct PPPSIntSlist_tag*   next;
};
typedef struct PPPSIntSlist_tag* PPPSIntSlist;



/*
	リストを作成

	戻り値：
		作成されたリスト。
		作成に失敗した場合は NULL が返される。
*/
PPPSIntSlist ppps_int_slist_create(void);

/*
	リストを破棄する

	引数：
		list:	破棄するリスト。
*/
void ppps_int_slist_delete(PPPSIntSlist list);

/*
	リストの末尾に要素を追加する

	引数：
		list:	追加対象のリスト。
		value:	追加する要素の値。
	戻り値：
		要素の追加に成功したら 0以外、失敗したら 0 が返される。
*/
bool ppps_int_slist_add_tail(PPPSIntSlist list, int value);

/*
	リストの先頭に要素を追加する

	引数：
		list:	追加対象のリスト。
		value:	追加する要素の値。
	戻り値：
		要素の追加に成功したら 0以外、失敗したら 0 が返される。
*/
bool ppps_int_slist_add_front(PPPSIntSlist list, int value);

/*
	リストに要素を挿入する

	引数：
		list:	追加対象のリスト。
		value:	追加する要素の値。
		pos:	挿入位置。これと同じ値を持つ要素の直後に挿入される。
	戻り値：
		要素の追加に成功したら 0以外、失敗したら 0 が返される。
*/
bool ppps_int_slist_insert(PPPSIntSlist list, int value, int pos);

/*
	要素を削除する

	引数：
		list:	追加対象のリスト。
		value：	削除する要素の値。
	戻り値：
		削除できた要素の個数を返す。
*/
int ppps_int_slist_delete_elem(PPPSIntSlist list, int value);

/*
	リストを空にする

	引数：
		list:	対象のリスト。
*/
void ppps_int_slist_clear(PPPSIntSlist list);

/*
	末尾の要素を探す

	引数：
		list:	対象のリスト。
	戻り値：
		連結リストの末尾にある要素を指すポインタ。
*/
PPPSIntSlist ppps_int_slist_search_tail(PPPSIntSlist list);

/*
	指定した値を持つ要素を探す

	引数：
		list:	対象のリスト。
		value：	探し出す要素の値。
	戻り値：
		先頭から連結リストを辿り、最初に見つけた value と同じ値を持つ要素のアドレス。
		見つからなかった場合は NULL を返す。
*/
PPPSIntSlist ppps_int_slist_search(PPPSIntSlist list, int value);

/*
	リスト内の要素の個数を返す

	引数：
		list:	対象のリスト。
	戻り値：
		リスト内に存在する要素の個数。
*/
int ppps_int_slist_count(PPPSIntSlist list);

/*
	リストを逆順にする。

	引数：
		list:	対象のリスト。
*/
void ppps_int_slist_reverse(PPPSIntSlist list);

/*
	要素を出力する

	引数：
		list:	対象のリスト。
*/
void ppps_int_slist_print(PPPSIntSlist list);


#endif
