#include "hash.h"
#include "ppps_int_slist.h"
#include <stdio.h>
#include <stdlib.h>

struct Hash_tag {
    PPPSIntSlist*    table; // hash_table
    size_t           size;  // data N
};

static size_t hash_func(Hash hash, int key);
static void* xmalloc(size_t size);

// hash_create
Hash hash_create(size_t size)
{
    struct Hash_tag* hash = xmalloc( sizeof(struct Hash_tag) );
    hash->table = xmalloc( sizeof(PPPSIntSlist) * size );
    for( size_t i = 0; i < size; ++i ){
        hash->table[i] = ppps_int_slist_create();
    }
    hash->size = size;

    return hash;
}

/*
    ハッシュ表を削除する
*/
void hash_delete(Hash hash)
{
    for( size_t i = 0; i < hash->size; ++i ){
        ppps_int_slist_delete( hash->table[i] );
    }
    free( hash->table );
    free( hash );
}

/*
    ハッシュ表にデータを追加する
*/
void hash_add(Hash hash, int data)
{
    size_t index = hash_func( hash, data );

    ppps_int_slist_add_front( hash->table[index], data );
}

/*
    ハッシュ表からデータを削除する
*/
void hash_remove(Hash hash, int data)
{
    size_t index = hash_func( hash, data );

    ppps_int_slist_delete_elem( hash->table[index], data );
}

/*
    ハッシュ表からデータを探索する
*/
int* hash_search(Hash hash, int data)
{
    size_t index = hash_func( hash, data );

    PPPSIntSlist elem = ppps_int_slist_search( hash->table[index], data );
    if( elem == NULL ){
        return NULL;
    }
    return &elem->value;
}




/*
    ハッシュ関数
*/
size_t hash_func(Hash hash, int key)
{
    return key % hash->size;
}

/*
    エラーチェック付きの malloc関数
*/
void* xmalloc(size_t size)
{
    void* p = malloc( size );
    if( p == NULL ){
        fputs( "メモリ割り当てに失敗しました。", stderr );
        exit( EXIT_FAILURE );
    }
    return p;
}