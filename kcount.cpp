#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>

#include "../dscgi-lib/include/argp.hpp"
#include "../dscgi-lib/include/seq.hpp"
#include "../dscgi-lib/include/kmer.hpp"
#include "../dscgi-lib/include/dhash.hpp"
#include "hash_oa.hpp"

using namespace std;
using namespace nog;

using SEQ = dnaseq<dvec<4>>;
using KMER = kmer_t<uint64_t>;
using KFUNC = kmer_func<uint64_t>;
//using TBL = unordered_map<KMER, uint32_t>;
//using TBL = dHashMap<KMER, uint32_t>;
using TBL = hash_oa<KMER, uint32_t, HashFNV_1a>;


void memory_usage ()
{
    ifstream ifs ("/proc/"+to_string (getpid ())+"/status");
    string s;
    while (!ifs.eof ()) {
        getline (ifs, s);
        if (s.find ("VmHWM") == 0 || s.find ("VmPeak") == 0) cerr << s << endl;
    }
}

void count_kmer (const SEQ& seq, TBL& table, int k, bool lower)
{
    static const KFUNC kf (k);
    static const uint64_t _defN = 0b0100 | (lower << 3);

    auto end = seq.begin ()+k-1;
    KMER fwd (seq.begin (), end);
    KMER rev = kf.to_revcomp (fwd);
    KMER masked (seq.begin (), end, _defN);

    auto itr = end; end = seq.end ();
    for (; itr != end; ++itr) {
        kf.push_back (fwd, *itr);
        kf.push_front (rev, complement (*itr));
        kf.push_back (masked, (*itr&_defN)>>2);
        if (masked) continue;

        // dhash or unordered_map
        // if (fwd < rev) ++table[fwd];
        // else           ++table[rev];

        // hash_op
        // static const KFUNC kf (32);
        // cerr << kf.to_string (fwd) << endl;

        // [] の参照の動作はやはり統一できたほうがよい 
        if (fwd < rev ) table.add(fwd);
        else            table.add(rev);

        // ハッシュ関数の計算負荷はかなり大きいので、できるだけ呼びたくない

    }
}

void output (TBL& table, int k)
{
    static const KFUNC kf (k);
    //for (auto& e: table) cout << kf.to_string (e.first) << "\t" << e.second << endl; // unordered_map
    //for (auto& e: table) cout << kf.to_string (e.key) << "\t" << e.value << endl; // dhash, ...
}

void init_args (argp& arg)
{
    arg.add_required<string> ("-file", "FASTA/FASTQ", "<FASTA>");
    arg.add_option<Range<int>> ("k", "k-mer size", {{1, 32}, 31});
    arg.add_option<bool> ("l,lower", "mask lowercases", true);
}

int main (int argc, char* argv[])
{
    argp arg;
    init_args (arg);
    arg.parse (argc, argv);

    TBL table;
    SEQ seq;

    iseqfile<SEQ> iseq (arg.value<string> ("-file"));

    while (iseq.read (seq)) {
        cerr << seq.name () << endl;
        count_kmer (seq, table, arg.value<int> ("k"), arg.value ("l"));
    }

    // output (table, arg.value<int> ("k"));

    cerr << "total_kmer = " << table.size() << endl;
    /*
    static const KFUNC kf (32);
    for (int i=0; i<table.size(); i++) {
        if ( table.get_value (i) != 0 ) {
            cout << kf.to_string (table.get_key (i)) << "\t" << table.get_value (i) << endl;
        }
    }
    */

    memory_usage ();

    return 0;
}
