
#include <vector>           // 動的配列
#include <unordered_map>    // ハッシュ
#include <iostream>         // 読み込み, 書き出し
#include <fstream>          // ファイル操作
#include <string>           // 文字列

#include "../klib/header/mlibFileReader2.h"
#include "../klib/header/mlibTimer.h"
#include "../klib/header/mlibString.h"

using namespace std;

class iris {
public:
    double sepal_l, sepal_w, petal_l, petal_w;
    string species;
    iris () {}
    iris (double sl, double sw, double pl, double pw, string s ) 
    : sepal_l (sl), sepal_w (sw), petal_l (pl), petal_w (pw), species (s) {}
    void dispOrig () {
        cout << sepal_l << "\t" << sepal_w << "\t" << petal_l << "\t" << petal_w << "\t" << species << "\n";
    }
};

class IRIS {
public:
    vector <string> header;
    vector <iris> irisS;
    IRIS () {}
    void add ( double sl, double sw, double pl, double pw, string s ) {
        irisS.push_back ( iris ( sl, sw, pl, pw, s ) );
    }
    void dispHeader () {
        for ( int i = 0; i < header.size() - 1; ++i ) {
            cout << header [i] << "\t";
        }
        cout << header.back() << "\n";
    }
    void dispOrig () {
        dispHeader ();
        for ( auto &i: irisS ) {
            i.dispOrig ();
        }
    }

};

/*
    ./irisReadClass2_fout.linux iris.txt iris.txt.out
*/
int main (int argc, char** argv) {

    string input, output;
    cerr << "**** start ****" << "\n";
    cerr << "argc = "       << argc << endl;
    cerr << "argv[0] = "    << argv[0] << endl;
    cerr << "argv[1] = "    << argv[1] << endl;
    cerr << "argv[2] = "    << argv[2] << endl;

    SequentialFileRead2 freader;
    bool nextOk = freader.openSingleFile ( argv[1] );
    vector <string> svec, header; 
    svec.reserve ( 30 );

    IRIS irisObjs;
    charBuf2vec::buf2stringVecSp ( header, freader.buf );
    nextOk = freader.getNext();
    irisObjs.header = header;

    while ( nextOk == true ) {
        charBuf2vec::buf2stringVec ( svec, freader.buf ); // 分解したいなら ; space* 区切りのも用意 = buf2stringVecSp
        irisObjs.add ( stod (svec[0]), stod (svec[1]), stod (svec[2]), stod (svec[3]), svec[4] );
        //irisObjs.irisS.back ().dispOrig ();
        nextOk = freader.getNext();
    }
    irisObjs.dispOrig ();

    cerr << "**** Finished ****" << endl;
    return 1;
}








//
