
#include <vector>           // 動的配列
#include <unordered_map>    // ハッシュ
#include <iostream>         // 読み込み, 書き出し
#include <fstream>          // ファイル操作
#include <string>           // 文字列

#include "../klib/header/mlibFileReader2.h"
#include "../klib/header/mlibTimer.h"
#include "../klib/header/mlibString.h"

using namespace std;

class iris {
public:
    double sepal_l, sepal_w, petal_l, petal_w;
    string species;
    iris () {}
    iris (double sl, double sw, double pl, double pw, string s ) 
    : sepal_l (sl), sepal_w (sw), petal_l (pl), petal_w (pw), species (s) {}
    void dispOrig () {
        cout << sepal_l << "\t" << sepal_w << "\t" << petal_l << "\t" << petal_w << "\t" << species << "\n";
    }
};

int main (int argc, char** argv) {

    string input, output;
    cerr << "**** start ****" << "\n";
    cerr << "argc = "       << argc << endl;
    cerr << "argv[0] = "    << argv[0] << endl;
    cerr << "argv[1] = "    << argv[1] << endl;
    cerr << "argv[2] = "    << argv[2] << endl;

    SequentialFileRead2 freader;
    bool nextOk = freader.openSingleFile ( argv[1] );
    vector <string> svec, header; // svec,
    svec.reserve ( 30 );

    charBuf2vec::buf2stringVecSp ( header, freader.buf );
    nextOk = freader.getNext();
    for (int i=0; i < header.size(); ++i )
        cout << header [i] << "\t";
    cout << "\n"; // [tab][\n] となるけど

    vector <iris> irisS;
    while ( nextOk == true ) {
        charBuf2vec::buf2stringVec ( svec, freader.buf ); // 分解したいなら ; space* 区切りのも用意 = buf2stringVecSp
        // iris tmp ( stod (svec[0]), stod (svec[1]), stod (svec[2]), stod (svec[3]), svec[4] );
        // tmp.dispOrig ();
        irisS.push_back ( iris ( stod (svec[0]), stod (svec[1]), stod (svec[2]), stod (svec[3]), svec[4] ) );
        irisS.back ().dispOrig ();
        nextOk = freader.getNext();
    }




    cerr << "**** Finished ****" << endl;
    return 1;
}








//
