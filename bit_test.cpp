#include <vector>
#include <unordered_map>
#include <iostream>
#include <fstream>
#include <string>
#include <bitset>

#include "../dscgi-lib/include/seq.hpp"

using namespace std;
using namespace nog;

using SEQ=dnaseq<dna_vector>;
// using SEQ=dnaseq<vector<char>>;

int main (int argc, char** argv) {

    /*
    cout <<  "(1 | 2) = " << (1 | 2) << endl; // b0001 OR  b0010 = b0011 
    //                                                       mask
    cout <<  "(15 & 3) = " << (15 & 3) << endl; // b1111 AND b0011 = b0011
    cout <<  "(1 ^ 2) = " << (1 ^ 2) << endl; // b0001 XOR b0010 = b0011
    cout <<  "(1 ^ 3) = " << (1 ^ 3) << endl; // b0001 XOR b0011 = b0010
    */

    SEQ seq;
    iseqfile<SEQ> iseq (argv[1]);
    iseq.read(seq); //,1,10);

    // A = 00
    // C = 01
    // G = 10
    // T = 11

    unsigned int kmer = 0; // 32bit
    kmer += 0b00; // A
    cout << "+A = "  << bitset<8>(kmer) << endl;
    kmer <<= 2;
    cout << "<<2 = " << bitset<8>(kmer) << endl;
    kmer += 0b11; // T
    cout << "+T = "  << bitset<8>(kmer) << endl;
    kmer <<= 2;
    cout << "<<2 = " << bitset<8>(kmer) << endl;
    kmer += 0b10; // G
    cout << "+G = "  << bitset<8>(kmer) << endl;

    cout << "seq.size() = " << seq.size() << endl;
    cout << seq.name() << endl;
    for (auto s:seq) {
        cout << dtoc (s);
    }
    cout << endl;

    unsigned int  mask1char = 0b11;
    unsigned int  genome_seg = 0b0;
    unsigned int  mask3char = 0b111111;
    unsigned long nowPos = 1;
    unsigned long nowCount = 0;
    for (auto s:seq) {
        //cout << bitset<8>(s) << endl;

        // 1. 下位2bit(ATGCの判別部分) しか使わないのでそれを取り出す
        // 2. それを genome_seg の一番右に詰める
        genome_seg |= (s & mask1char);
        // 3. 下位3文字を取り出す
        genome_seg &= mask3char;

        // お試し出力
        // cout << nowPos++ << " : " << bitset<8>(genome_seg) << endl;

        // 4. kmer(ATG) と称号
        if ( genome_seg == kmer) {
            cout << ++nowCount << " : " << nowPos << " : " << bitset<8>(genome_seg) << endl;
        }
        nowPos++;

        // 4. 1文字(2bit)ずらす
        genome_seg <<= 2;

    }

}

