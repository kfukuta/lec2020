
#include <vector>
#include <unordered_map>
#include <iostream>
#include <fstream>
#include <string>

#include "../klib/header/mlibFileReader2.h"
#include "../klib/header/mlibTimer.h"
#include "../klib/header/mlibString.h"

#include "../dscgi-lib/include/argp.hpp"

// mlibFileReader を使った fasta 読み込みサンプル

using namespace std;
using namespace nog;

// ASCII
//  A=65, C=67, G=71,  N=78,  T=84
//  a=97, c=99, g=103, n=110, t=116
// A = 0000 0000 ; A = 0
// T = 0001 0000 ; A + 16 = 16
// C = 0000 0001 ; C = 1
// G = 0001 0001 ; C + 16 = 17
// a = 0010 0000 ; A      + 32 = 32
// t = 0011 0000 ; A + 16 + 32 = 48
// c = 0010 0001 ; C      + 32 = 33
// g = 0011 0001 ; C + 16 + 32 = 49
// N = xx0x xxxx ; 6bit==0 && AGCT 以外 = 64      = 0100 0000
// n = xx1x xxxx ; 6bit==1 && acgt 以外 = 64 + 32 = 0110 0000

// 8bit = 4char
// 00 a 11
// 01 g 10
// 10 c
// 11 t
                //      0   1   2   3   4   5   6   7   8   9
static int char2ind2[128]  = { 
                        64, 64, 64, 64, 64, 64, 64, 64, 64, 64, //   0+ 
                        64, 64, 64, 64, 64, 64, 64, 64, 64, 64, //  10+
                        64, 64, 64, 64, 64, 64, 64, 64, 64, 64, //  20+
                        64, 64, 64, 64, 64, 64, 64, 64, 64, 64, //  30+
                        64, 64, 64, 64, 64, 64, 64, 64, 64, 64, //  40+
                        64, 64, 64, 64, 64, 64, 64, 64, 64, 64, //  50+
                        64, 64, 64, 64, 64,  0, 64,  1, 64, 64, //  60+ A65 以降のAGCTN 以降のコードは N
                        64, 17, 64, 64, 64, 64, 64, 64, 64, 64, //  70+
                        64, 64, 64, 64, 16, 64, 64, 64, 64, 64, //  80+
                        64, 64, 64, 64, 64, 64, 64, 32, 96, 33, //  90+ a97 以降のagctn 以外のコードは n 
                        96, 96, 96, 49, 96, 96, 96, 96, 96, 96, // 100+
                        96, 96, 96, 96, 96, 96, 48, 96, 96, 96, // 110+
                        96, 96, 96, 96, 96, 96, 96, 96          // 120+
                    }; 

                //   0   1   2   3   4   5   6   7   8   9
static char ind2char[128] =  {
                    'A','C','N','N','N','N','N','N','N','N',  //   0+ 
                    'N','N','N','N','N','N','T','G','N','N',  //  10+ 
                    'N','N','N','N','N','N','N','N','N','N',  //  20+ 
                    'N','N','a','c','N','N','N','N','N','N',  //  30+ 
                    'N','N','N','N','N','N','N','N','N','N',  //  40+ 
                    'N','N','N','N','N','N','N','N','t','g',  //  50+ 
                    'N','N','N','N','n','n','n','n','n','n',  //  60+ 
                    'n','n','n','n','n','n','n','n','n','n',  //  70+ 
                    'n','n','n','n','n','n','n','n','n','n',  //  80+ 
                    'n','n','n','n','n','n','n','n','n','n',  //  90+ 
                    'n','n','n','n','n','n','n','n','n','n',  // 100+ 
                    'n','n','n','n','n','n','n','n','n','n',  // 110+ 
                    'n','n','n','n','n','n','n','n'           // 120+ 
                    };


class faStat {
public:
    faStat(){};
    int ntCount[128] = {0};

    static int char2nt ( char _c ) {
        return char2ind2 [ _c ];
    }

    void countAdd ( vector<char> _seq ) {
        for ( auto &c: _seq ) {
            ntCount [ char2nt ( c ) ]++;
        }
    }

    void dispNtCount ( auto& out) {
        out << "A = " << ntCount [0]  << "\n";
        out << "C = " << ntCount [1]  << "\n";
        out << "G = " << ntCount [17] << "\n";
        out << "T = " << ntCount [16] << "\n";
        out << "N = " << ntCount [64] << "\n"; 

        out << "a = " << ntCount [32]  << "\n";
        out << "c = " << ntCount [33]  << "\n";
        out << "g = " << ntCount [49] << "\n";
        out << "t = " << ntCount [48] << "\n";
        out << "n = " << ntCount [96] << "\n";
    }

    //void char2comp

};


class seqTrans {
public:
    seqTrans () {};

    static int char2nt ( char _c ) {
        return char2ind2 [ _c ];
    }
    void comp ( vector<char> _seq ) {
        for ( auto &c: _seq ) {
            cout << char2nt ( c );
            //cout << idx2char ( char2nt ( c ) );
        }
        cout << endl;
    }

    /*
    void idx2char ( int _idx ) {
        return ind2char [ _idx ];
    }
    */
};



class fasta {
public:
    vector<char>    seq; // seq[0] ; seq[99]
    string          header;
    string          id;
    fasta () {}
    fasta (vector<char> _seq, string _header ) 
        : seq (_seq), header (_header) {
        set_id ( header );
    }
    void set_id ( string _header ) {
        // full-header to header-id
        // ...
        id = _header;
    }

    void dispOrig ( auto &out ) {
        out << header << "\n";
        dispSeq ( out );
    }

    void dispOrigChar ( auto &out ) {
        out << header << "\n";
        dispSeqChar ( out );
    }

    void dispOrigSeqNtIdx ( auto &out ) {
        out << header << "\n";
        dispSeqNtIdx ( out );
    }


private:
    void dispSeq ( auto &out ) {
        for ( auto &c: seq ) {
            out << c;
        }
        cout << "\n";
    }

    void dispSeqChar ( auto &out ) {
        for ( auto &c: seq ) {
            out << "c" << (int)c;
        }
        cout << "\n";
    }

    void dispSeqNtIdx ( auto &out ) {
        for ( auto &c: seq ) {
            out << "i" << faStat::char2nt( c );
        }
        cout << "\n";
    }

};




class mfasta {
public:
    vector <fasta> SEQ; // SEQ[0]
    mfasta () { } 
    mfasta ( string inFile ) {
        readFasta ( SEQ, inFile );
    }

    size_t size () { return SEQ.size(); }

public:
	fasta & operator [](int n) { return SEQ [n]; } // ++ << >> * -> -- 

private:
    void readFasta ( vector<fasta> &SEQ, string input ) {

        SequentialFileRead2 freader;
        bool nextOk = freader.openSingleFile ( input );
        string headerTmp;
        vector<char> seqTmp;
        seqTmp.reserve ( 1024 * 1024 * 256 );

        while ( nextOk == true ) {
            if ( freader.buf[0] == '>' ) {
                if ( seqTmp.size() != 0 ) {
                    SEQ.push_back ( fasta ( seqTmp, headerTmp ) );
                    // SEQ.back().dispOrig ( cout );
                }
                headerTmp = freader.buf;
                headerTmp.erase ( headerTmp.size() - 1 ); // 末尾の削除
                seqTmp.clear ();
            } else {
                for (auto &c: freader.buf) {
                    if (c != '\n') {
                        seqTmp.push_back (c);
                    } else {
                        break;
                    }
                }
            }
            nextOk = freader.getNext();
        }

        if ( seqTmp.size() != 0 ) {
            SEQ.push_back ( fasta ( seqTmp, headerTmp ) );
            // SEQ.back().dispOrig ( cout );
        }

    }

};






int main (int argc, char** argv) {

    // if ( argc == 1 ) {
    //     cerr << "ex)" << endl;
    //     cerr << "  ./fastaRead.linux target.fa " << endl;
    //     cerr << "  cat target.fa | ./fastaRead.linux - " << endl;
    //     return 0;
    // }
    // cerr << "**** start ****" << "\n";
    // cerr << "argc = "       << argc << endl;
    // cerr << "argv[0] = "    << argv[0] << endl; // self(command)
    // cerr << "argv[1] = "    << argv[1] << endl;

    argp arg ("fastaRead2.linux", "v0.01");
    arg.add_required<string> ("f,fasta", "fasta path");
    arg.parse (argc, argv);

    Timer _timer;

    //mfasta mfa ( argv[1] );
    mfasta mfa ( arg.value<string> ("fasta") );

    // mfa.sort ();
    // cout << mfa.get_n50 () << endl;

    //---------------------------------------------------------------------------------
    // for ( auto &s: mfa.SEQ ) {
    //     s.dispOrig ( cout );
    // }
    // /*
    faStat stat;
    for ( int i=0; i<mfa.size(); i++) {
        //mfa[i].dispOrig (cout);
        //mfa[i].dispOrigChar (cout);
        //mfa[i].dispOrigSeqNtIdx (cout);
        stat.countAdd ( mfa[i].seq );
    }

    stat.dispNtCount (cout);

    // */


    cerr << "seq.count = " << mfa.size() << endl;

    cerr << "----------------------------------------" << endl;
    mfa[0].dispOrig (cout);
    mfa[0].dispOrigChar (cout);
    mfa[0].dispOrigSeqNtIdx (cout);
    cerr << "----------------------------------------" << endl;
    seqTrans st;
    st.comp ( mfa[0].seq );

    _timer.dispR();


    cerr << "**** Finished ****" << endl;
    return 1;
}








//
