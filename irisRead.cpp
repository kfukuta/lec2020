
#include <vector>           // 動的配列
#include <unordered_map>    // ハッシュ
#include <iostream>         // 読み込み, 書き出し
#include <fstream>          // ファイル操作
#include <string>           // 文字列

#include "../klib/header/mlibFileReader2.h"
#include "../klib/header/mlibTimer.h"
#include "../klib/header/mlibString.h"

using namespace std;

int main (int argc, char** argv) {

    string input, output;
    cerr << "**** start ****" << "\n";
    cerr << "argc = "       << argc << endl;
    cerr << "argv[0] = "    << argv[0] << endl;
    cerr << "argv[1] = "    << argv[1] << endl;
    cerr << "argv[2] = "    << argv[2] << endl;

    SequentialFileRead2 freader;
    bool nextOk = freader.openSingleFile ( argv[1] );
    vector <string> svec, header; // svec,
    svec.reserve ( 30 );

    charBuf2vec::buf2stringVecSp ( header, freader.buf );
    nextOk = freader.getNext();
    for (int i=0; i < header.size(); ++i )
        cout << header [i] << "\t";
    cout << "\n"; // [tab][\n] となるけど

    string tmp;
    while ( nextOk == true ) {
        // cout << freader.buf; // そのまま読み込み結果を出すならこれでもいい
        charBuf2vec::buf2stringVec ( svec, freader.buf ); // 分解したいなら ; space* 区切りのも用意 = buf2stringVecSp
        for (int i=0; i < svec.size(); ++i )
            cout << svec [i] << "\t";
        cout << "\n"; // [tab][\n] となるけど

        nextOk = freader.getNext();
    }




    cerr << "**** Finished ****" << endl;
    return 1;
}








//
