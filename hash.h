// hash.h

#ifndef HASH_H_INCLUDED
#define HASH_H_INCLUDED

typedef struct Hash_tag* Hash;

/*
    ハッシュ表を作る

    引数：
        size:   ハッシュ表の大きさ
    戻り値：
        作成されたハッシュ表。
        使い終わったら、hash_delete関数に渡して削除する。
*/
Hash hash_create(size_t size);

/*
    ハッシュ表を削除する

    引数：
        hash:   hash_create関数で作成したハッシュ表
*/
void hash_delete(Hash hash);

/*
    ハッシュ表にデータを追加する

    引数：
        hash:   hash_create関数で作成したハッシュ表
        data:   追加するデータ
*/
void hash_add(Hash hash, int data);

/*
    ハッシュ表からデータを削除する

    引数：
        hash:   hash_create関数で作成したハッシュ表
        data:   削除するデータ
*/
void hash_remove(Hash hash, int data);

/*
    ハッシュ表からデータを探索する

    引数：
        hash:   hash_create関数で作成したハッシュ表
        data:   探索するデータ
    戻り値：
        ハッシュ表の要素へのポインタ。
        見つからなければ NULL。
*/
int* hash_search(Hash hash, int data);

#endif