#pragma once
#include "../dscgi-lib/include/hash_functions.hpp"

// kmer の格納に限定
// 削除はしない
// val=0 を空とする
// a. 線形走査法 
//      1. シンプル1 : +1,+1,+1,...,max
//      2. パターン1 : return x2 ( 1,2,4,8,...,max )
// b. 二重ハッシュ法
//      1. hash_valu1 と second_hash を互いに素
//      2. ...

namespace nog {

    template <class T_key, class T_val, class T_hasher = HashFNV_1a>
    class hash_oa {

        private:
            struct bucket {
                T_key key;
                T_val value;
                bucket (): key {}, value {} {}
                bool is_empty () {
                    return value == 0;
                }
            };

            std::vector<bucket> _table;
            using bucket_itr_1d = typename std::vector<bucket>::iterator; // dhash の名前付けを参考、参照のこと。

            static constexpr uint64_t  bcount_init = 4194304ull; // 4^11 // SEARCH_SPACE分の余剰を含ませる // bkt_size0
            static constexpr uint64_t  SEARCH_SPACE = 128; // 256, 512
            uint64_t        bcount; // 現在の箱の総数 ; bkt_size
            uint64_t        bsize;  // 値が入っている箱の総数
            T_hasher        _hasher;
            // int a = 10;

        public:
            class iterator : public std::iterator<std::forward_iterator_tag, bucket> {
                bucket_itr_1d itr1, end1;
            public:
                iterator () {}
                //iterator ( bucket_itr_1d ed1 ) : end1 (ed1) {}
                iterator ( bucket_itr_1d it1, bucket_itr_1d ed1 ) : itr1 (it1), end1 (ed1) {}
                bucket& operator*() const { return *itr1; }
                bool operator!=(const iterator& x) const { return ( itr1 != x.itr1 ); }
                iterator& operator++() { // hash の次の箱へのアクセスは、キーの有無の確認が必要 ; hash値の +1 に実際の値があるかどうかはわからない
                    while (1) {
                        if (itr1 == end1) break;
                    }
                    return *this;
                }
            };
            iterator begin ( ) { // 値が入っている bucket のポインタをかえせばいい
                bucket_itr_1d it1 = _table.begin ();
                bucket_itr_1d ed1 = _table.end ();
                return iterator (it1, ed1);
            }
            iterator end ( ) { // 値が入っている末尾の bucket のポインタをかえせばいい
                bucket_itr_1d ed1 = _table.end (); // _table は vector<bucket> つまり、vector なので、beginもendも最初からある
                return iterator (ed1, ed1);
            }
            iterator& operator++() {
                ++itr1;
                while (1) {
                    if (! (*itr).empty()) return *this; //空でない、hash値がはいっているならそれを。
                    ++itr1;
                    if (itr1==end1) break; // 最後なら最後を。
                }
                return *this;
            }
            // .value == T_val() で空のインスタンスを作れて、それで比較


        private:
            uint64_t get_hash ( const T_key& key ) { return _hasher (key) % bcount; }; // &mask に。
            T_val& bval_search ( const T_key& key ) { // 見るべき bucket 番号を探し、値を得る( val==0は空 )
                uint64_t bnum = get_hash ( key );
                while (1) { // 削除フラグは存在せず、val==0 が空&&検索の終端なので、これをフラグに検索
                    if ( _table [bnum].key == key || _table [bnum].is_empty() )
                        break;
                    else
                        ++bnum;
                }
                return _table [bnum].value;
            }
            inline void _rehash ( uint64_t newSize ) {
                std::cerr << "Exe: hash_oa._rehash(): newSize = " << newSize << "\n";

                bcount = newSize;
                // bsize の数は新しい table でも変わらない。
                // shift を使ったバージョンはおかしくなったので通常版で
                std::vector<bucket> old ( _table.size() ); // 課題: 1.5倍のメモリの使用について
                old.swap ( _table );
                _table.resize ( newSize );
                _table.clear ();
                for ( auto& now_bucket: old ) {
                    if ( now_bucket.is_empty () )
                        continue;
                    else {
                        uint64_t bnum = get_hash ( now_bucket.key );
                        _table [bnum].key = now_bucket.key;
                        _table [bnum].value = 1;
                    }
                }
            }

        public:
            hash_oa (): bcount ( bcount_init ), bsize{} { 
                _table.reserve ( bcount );
            }
            T_val& operator[] ( const T_key& key ) { // == search ()
                // テーブルから value を取り出す
                // ** value==0 では何もしない -> 参照としてしか機能しない **
                return bval_search ( key );
            }
            uint64_t size () { return bsize; } // 空含まない
            T_key get_key ( uint64_t tag_bnum ) {
                return _table [tag_bnum].key;
            }
            T_val get_value ( uint64_t tag_bnum ) {
                return _table [tag_bnum].value;
            }

            // bucketの T_val リファレンスを返すようにする
            // T_val& add()
            void add ( const T_key& key ) { // value を加算する。なければ1を入れる。
                // valは val++ (val==0 は 1) ;
                uint64_t bnum = get_hash ( key );
                uint64_t search_dist = 0;
                while (1) {
                    if ( _table [bnum].is_empty() ) {
                        //std::cerr<< "chk01, bnum = " << bnum << std::endl;
                        _table [bnum].key = key;
                        _table [bnum].value = 1;
                        ++bsize;
                        return; // _table [bnum].value;
                    } else if ( _table [bnum].key == key ) {
                        //std::cerr<< "chk02, bnum = " << bnum << std::endl;
                        ++_table [bnum].value;
                        return; // _table [bnum].value;
                    }

                    // 線形走査法による衝突回避
                    ++search_dist;
                    if ( search_dist > SEARCH_SPACE || bnum >= bcount ) {
                            // 1. 挿入範囲内が全部うまっていたらリハッシュする
                            // 2. 末端ギリギリにあるなら即リハッシュだとギリギリでの拡大が微妙では、、。別スペース用意したほうがいいのでは？
                        _rehash ( bcount * 2 ); // 現在の箱の大きさを2倍にする
                        bnum = get_hash ( key );
                        search_dist = 0;
                        //std::cerr<< "chk03, rehash, bnum = " << bnum << std::endl;
                        continue;
                    } else {
                        ++bnum;
                    }
                }
            }


    };

}

