
#include <vector>
#include <unordered_map>
#include <iostream>
#include <fstream>
#include <string>

#include "../klib/header/mlibFileReader2.h"
#include "../klib/header/mlibTimer.h"
#include "../klib/header/mlibString.h"

// mlibFileReader を使った fasta 読み込みサンプル

using namespace std;

class faseq {
public:
    vector<char>    seq;
    string          header;
    string          id;
    faseq () {}
    faseq (vector<char> _seq, string _header ) 
        : seq (_seq), header (_header) {
        set_id ( header );
    }
    void set_id ( string _header ) {
        // full-header to header-id
        // ...
        id = _header;
    }

    void dispOrig ( auto &out ) {
        out << header << "\n";
        dispSeq ( out );
    }

private:
    void dispSeq ( auto &out ) {
        for ( auto &c: seq ) {
            out << c;
        }
        cout << "\n";
    }

};


int main (int argc, char** argv) {

    string input, output;

    if ( argc == 1 ) {
        cout << "ex)" << endl;
        cout << "  ./fastaRead.linux target.fa " << endl;
        cout << "  cat target.fa | ./fastaRead.linux - " << endl;
        return 0;
    }

    cerr << "**** start ****" << "\n";
    cerr << "argc = "       << argc << endl;
    cerr << "argv[0] = "    << argv[0] << endl; // self(command)
    cerr << "argv[1] = "    << argv[1] << endl;

    Timer _timer;

    vector<faseq> SEQ;

    //---------------------------------------------------------------------------------
    SequentialFileRead2 freader;
    bool nextOk = freader.openSingleFile ( argv[1] );
    string seqTmp, headerTmp;
    while ( nextOk == true ) {
        if ( freader.buf[0] == '>' ) {
            if ( seqTmp.size() != 0 ) {
                SEQ.push_back ( faseq ( vector<char> ( seqTmp.begin(), seqTmp.end() ), headerTmp ) );
                //SEQ.back().dispOrig ( cout );
            }
            headerTmp =  freader.buf;
            headerTmp.erase ( headerTmp.size() - 1 ); // 末尾の削除
            seqTmp = "";
        } else {
            string origBuf =  freader.buf;
            if ( origBuf.back() == '\n' ) {
                origBuf.erase ( origBuf.size() - 1 ); // 末尾の削除
            }
            seqTmp += origBuf;
        }
        nextOk = freader.getNext();
    }

    if ( seqTmp.size() != 0 ) {
        SEQ.push_back ( faseq ( vector<char> ( seqTmp.begin(), seqTmp.end() ), headerTmp ) );
        //SEQ.back().dispOrig ( cout );
    }

    cout << "seq.count = " << SEQ.size() << endl;


    // irisObjs.dispOrig ( fout );
    _timer.dispR();


    cerr << "**** Finished ****" << endl;
    return 1;
}








//
