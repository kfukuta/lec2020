
#include <vector>           // 動的配列
#include <unordered_map>    // ハッシュ
#include <iostream>         // 読み込み, 書き出し
#include <fstream>          // ファイル操作
#include <string>           // 文字列

#include "../klib/header/mlibFileReader2.h"
#include "../klib/header/mlibTimer.h"
#include "../klib/header/mlibString.h"

using namespace std;

class iris {
public:
    double sepal_l, sepal_w, petal_l, petal_w;
    string species;
};


void dispOrig ( double &_sl, double &_sw, double &_pl, double &_pw, string &_s,       unsigned int recN = 0 ) {
    if (recN > 0 )
        cout << recN << "\t" << _sl << "\t" << _sw << "\t" << _pl << "\t" << _pw << "\t" << _s << "\n";
    else 
        cout << _sl << "\t" << _sw << "\t" << _pl << "\t" << _pw << "\t" << _s << "\n";
}

void func ( int a, int b, bool c = true ) {

}

void dispOrig ( auto& ofs, iris &_iRec,     unsigned int recN = 0 ) {
    if (recN > 0 )
        ofs << recN << "\t" << _iRec.sepal_l << "\t" << _iRec.sepal_w << "\t" 
                             << _iRec.petal_l << "\t" << _iRec.petal_w << "\t" << _iRec.species << "\n";
    else 
        ofs << _iRec.sepal_l << "\t" << _iRec.sepal_w << "\t" 
             << _iRec.petal_l << "\t" << _iRec.petal_w << "\t" << _iRec.species << "\n";
}


int main (int argc, char** argv) {

    string input, output;

    if ( argc == 1 ) {
        cout << "ex)" << endl;
        cout << "  ./irisRead_simple.linux iris.txt iris.txt.out" << endl;
        return 0;
    }

    cerr << "**** start ****" << "\n";
    cerr << "argc = "       << argc << endl;
    cerr << "argv[0] = "    << argv[0] << endl; // com
    cerr << "argv[1] = "    << argv[1] << endl; // input_file_name
    cerr << "argv[2] = "    << argv[2] << endl; // out_file_name

    Timer _timer;

    //---------------------------------------------------------------------------------
    // フォーマットがわかっているので決め打ちして読ませる
    // ifstream 及び、型変換で istringstream を使った例。列数はわかっているので決め打ちで変換して、前の例と同様のクラスを作る
    ofstream fout0 ( (string) argv[2] + ".raw" );

    ifstream file ( argv[1] );
    string lbuf, lheader;
    getline ( file, lheader ); // ヘッダーは捨てた
    fout0 << lheader << "\n";
    cout << lheader << "\n";

    vector<iris> irisRecs;
    unordered_map<string, vector<unsigned int>> sp2recss;
    unsigned int recN {0};
    while ( 1 ) {
        iris irisTmp;
        file >> irisTmp.sepal_l >> irisTmp.sepal_w >> irisTmp.petal_l >> irisTmp.petal_w >> irisTmp.species;
        sp2recss [irisTmp.species].push_back (recN);
        irisRecs.push_back ( irisTmp );
        dispOrig ( cout, irisRecs.back(), ++recN );
        dispOrig ( fout0, irisRecs.back() );

        if ( file.eof() ) 
            break;
    }

    for ( auto itr = sp2recss.begin(); itr != sp2recss.end(); ++itr) {
        std::cout << "key = " << itr->first
                  << ", recN = " << itr->second.size() << "\n";
    }

    _timer.dispR();

    vector<unsigned int> targetRecs = sp2recss["virginica"];
    for ( auto& r: targetRecs ) {
        //cout << r << "\n";
        dispOrig ( cout, irisRecs [r] );
    }

    _timer.dispR();

    cerr << "Write = " << argv[2] << "\n";
    cerr << "**** Finished ****" << endl;

    _timer.dispF();

    return 1;
}








//
