
#include <vector>           // 動的配列
#include <unordered_map>    // ハッシュ
#include <iostream>         // 読み込み, 書き出し
#include <fstream>          // ファイル操作
#include <string>           // 文字列

#include "../klib/header/mlibFileReader2.h"
#include "../klib/header/mlibTimer.h"
#include "../klib/header/mlibString.h"

using namespace std;

class iris {
public:
    double sepal_l, sepal_w, petal_l, petal_w;
    string species;
    iris () {}
    iris (double sl, double sw, double pl, double pw, string s ) 
    : sepal_l (sl), sepal_w (sw), petal_l (pl), petal_w (pw), species (s) {}
    void dispOrig ( auto &out ) {
        out << sepal_l << "\t" << sepal_w << "\t" << petal_l << "\t" << petal_w << "\t" << species << "\n";
    }
};

class IRIS {
public:
    vector <string> header;
    vector <iris> irisS;
    IRIS () {}
    void add ( double sl, double sw, double pl, double pw, string s ) {
        irisS.push_back ( iris ( sl, sw, pl, pw, s ) );
    }
    void dispHeader ( auto &out ) {
        for ( int i = 0; i < header.size() - 1; ++i ) {
            out << header [i] << "\t";
        }
        out << header.back() << "\n";
    }
    void dispOrig ( auto &ofs ) {
        dispHeader ( ofs );
        for ( auto &i: irisS ) {
            i.dispOrig ( ofs );
        }
    }

};

int main (int argc, char** argv) {

    string input, output;

    if ( argc == 1 ) {
        cout << "ex)" << endl;
        cout << "  ./irisReadClass2_fout.linux iris.txt iris.txt.out" << endl;
        cout << "  cat iris.txt | ./irisReadClass2_fout.linux - iris.txt.out" << endl;
        return 0;
    }

    cerr << "**** start ****" << "\n";
    cerr << "argc = "       << argc << endl;
    cerr << "argv[0] = "    << argv[0] << endl; // com
    cerr << "argv[1] = "    << argv[1] << endl; // input_file_name
    cerr << "argv[2] = "    << argv[2] << endl; // out_file_name

    Timer _timer;

    //---------------------------------------------------------------------------------
    SequentialFileRead2 freader;
    bool nextOk = freader.openSingleFile ( argv[1] );
    vector <string> svec, header;

    ofstream fout (argv[2]);

    IRIS irisObjs;
    charBuf2vec::buf2stringVecSp ( header, freader.buf );
    nextOk = freader.getNext();
    irisObjs.header = header;

    double a2, b2, c2, d2;
    string e2;
    while ( nextOk == true ) {
        // cout << freader.buf << endl;
        charBuf2vec::buf2stringVec ( svec, freader.buf ); // 分解したいなら ; space* 区切りのも用意 = buf2stringVecSp
        // irisObjs.add ( stod (svec[0]), stod (svec[1]), stod (svec[2]), stod (svec[3]), svec[4] );
        //irisObjs.irisS.back ().dispOrig ();
        a2 = stod (svec[0]);
        b2 = stod (svec[1]);
        c2 = stod (svec[2]);
        d2 = stod (svec[3]);
        e2 = svec[4]; // 好きな列だけ変換して使える

        nextOk = freader.getNext();
    }
    // irisObjs.dispOrig ( fout );
    _timer.dispR();

    //---------------------------------------------------------------------------------
    // ifstream 及び、型変換で istringstream を使った例。列数はわかっているので決め打ちで変換して、前の例と同様のクラスを作る
    IRIS irisObjs0;
    ofstream fout0 ( (string) argv[2] + ".raw" );
    ifstream file ( argv[1] );
    string lbuf, lheader;
    getline ( file, lheader );
    irisObjs0.header = header; // vector<string>の header 作るの面倒なのですでに作ってある方をコピーしてイカサマする
    double a, b, c, d;
    string e;
    while ( getline ( file, lbuf ) ) {
        // cout << lbuf << endl;
        istringstream iss ( lbuf );
        iss >> a >> b >> c >> d >> e; // a,e だけを実際には使いたい場合も、このコードでは b,c,d における処理が行われてしまう。
        // irisObjs0.add ( a, b, c, d, e );
        // cout << a << "\t" << b << "\t" << c << "\t" << e << "\n";
    }
    // irisObjs0.dispOrig ( fout0 );
    _timer.dispR();

    // 全ての列を読み込む（tabで区切って適切な型変換を行う）場合はさほど速度に差がでないが、それでも SequentialFileRead2 のほうが早い
    // 分解と型変換を istringstream にまかせた場合は、1列目と3列目だけ使うといった制御がしづらい
    // 対して、前者は任意の列だけを自由に使用して特定のクラスを作ることができる

    cerr << "Write = " << argv[2] << "\n";
    cerr << "**** Finished ****" << endl;
    return 1;
}








//
