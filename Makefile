
ENV := linux

DIR1  := irisRead_simple.$(ENV) irisRead_simple2.$(ENV) kmer_count.$(ENV) kcount.$(ENV)
DIR2  := irisReadClass.$(ENV) irisRead.$(ENV)
DIR3  := irisReadClass2.$(ENV)
DIR4  := irisReadClass2_fout.$(ENV)
DIR5  := fastaRead.$(ENV) fastaRead2.$(ENV) bit_test.$(ENV)
EXE := $(DIR1) $(DIR2) $(DIR3) $(DIR4) $(DIR5)

#----------------------------------------------------
# gcc
CCB := g++ -O3 -std=c++14 -lstdc++fs -fPIC -Wno-unused-result -fconcepts
CCF := -lboost_iostreams -pthread -fopenmp

#----------------------------------------------------
ESRCS := $(EXE:%.$(ENV)=%.cpp)
EOBJS := $(ESRCS:%.cpp=%.o)
EDEPS := $(ESRCS:%.cpp=%.d)

all: $(EXE)
-include $(EDEPS)

%.o: %.cpp
	$(CCB) -c -MMD $< -o $(<:%.cpp=%.o) $(CCF)

define mainCompile
TAG := $1
$(TAG): $(TAG:%.$(ENV)=%.o)
	$(CCB) -o $(TAG) $(TAG:%.$(ENV)=%.o) $(CCF)
endef

DAMMY := dammy
EXE2 := $(EXE) $(DAMMY)
$(foreach name, $(wordlist 1,$(words $(EXE2)),$(EXE2)),$(eval $(call mainCompile,$(name))))


# make use
use:
	@echo "make clean"
	@echo "make [-j thread]"

clean:
	find . -name "*.o" | xargs rm
	find . -name "*.d" | xargs rm
	find . -name "*.linux" | xargs rm

list:
	find . -name "*linux"



#//
